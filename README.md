# Commerce Stock Notifications

The Commerce Stock Notifications module modifies the "Add to Cart"
form on products that are out of stock, allowing the users to enter an
e-mail address to be notified when the product is back in stock.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/commerce_stock_notifications).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/commerce_stock_notifications).

## Requirements

Commerce Stock Notifications requires the following modules:

- [Commerce](https://www.drupal.org/project/commerce)
- [Commerce Product](https://www.drupal.org/project/commerce)
- [Commerce Stock](https://www.drupal.org/project/commerce_stock)
- [Commerce Stock Field](https://www.drupal.org/project/commerce_stock)

## Recommended modules

- [Swift Mailer](https://www.drupal.org/project/swiftmailer)

## Installation

Install as you would normally install a contributed Drupal module. Visit
[installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules)
for further information.

## Configuration

Configure the notification form messages in Commerce » Configuration » Stock
Notification.

If you want to allow anonymous users to subscribe for notifications, grant
them the permission "Create commerce stock notifications". Note that they
won't be able to unsubscribe and you will have to unsubscribe them manually.

## Maintainers

Current maintainers: see the [project page](https://www.drupal.org/project/commerce_stock_notifications).

This project was sponsored by:
 * Studio Present (https://www.studiopresent.com)
