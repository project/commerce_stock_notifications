<?php

namespace Drupal\commerce_stock_notifications\Plugin\QueueWorker;

use Drupal\commerce_stock_notifications\Form\AdminConfigForm;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Component\Utility\DeprecationHelper;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Utility\Token;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends out notifications when items are in stock.
 *
 * @QueueWorker(
 *   id = "commerce_stock_notifications",
 *   title = @Translation("Commerce Stock Notification Queue"),
 *   cron = {"time" = 120}
 * )
 */
class CommerceStockNotifyQueue extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  use StringTranslationTrait;

  /**
   * Mail service for sending out the mail.
   *
   * @var \Drupal\Component\Plugin\PluginManagerInterface
   */
  private $mailManager;

  /**
   * Entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Token.
   *
   * @var \Drupal\Core\Utility\Token
   */
  private $token;

  /**
   * The renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  private $renderer;

  /**
   * CommerceStockNotifyQueue constructor.
   *
   * @param array $configuration
   *   Configuration.
   * @param mixed $plugin_id
   *   Plugin ID.
   * @param mixed $plugin_definition
   *   Plugin Definition.
   * @param \Drupal\Core\Mail\MailManagerInterface $mailManager
   *   Mail manager.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   Entity type manager.
   * @param \Drupal\Core\Utility\Token $token
   *   Token.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, MailManagerInterface $mailManager, EntityTypeManagerInterface $entityTypeManager, Token $token, RendererInterface $renderer) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->mailManager = $mailManager;
    $this->entityTypeManager = $entityTypeManager;
    $this->token = $token;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.mail'),
      $container->get('entity_type.manager'),
      $container->get('token'),
      $container->get('renderer')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    if (empty($data->get('sent_time')->getValue())) {
      /** @var \Drupal\user\UserInterface $user */
      $user = user_load_by_mail($data->getEmail());

      /** @var \Drupal\commerce_product\Entity\ProductVariationInterface $variation */
      $variation = $this->entityTypeManager->getStorage('commerce_product_variation')->load($data->get('product_id')->getString());
      if (!$variation->isPublished() || !$variation->getProduct()->isPublished()) {
        $sku = $variation->getSku();
        // The product or variation is inactive, ignore it.
        throw new \Exception("Variation $sku (or it\'s parent product) is unpublished. Skipping for now.");
      }
      if (!commerce_stock_notifications_check_stock($variation)) {
        // There is no stock right now, do nothing.
        return;
      }

      // Parameters that will be used during the mailing procedure.
      $subject = $this->token->replace(AdminConfigForm::getValuesFromConfig()['email_subject'], [
        'commerce_product_variation' => $variation,
        'user' => $user,
      ]);
      $message = $this->token->replace(AdminConfigForm::getValuesFromConfig()['email_body'], [
        'commerce_product_variation' => $variation,
        'user' => $user,
      ]);
      $build = [
        '#theme' => 'commerce_stock_notifications_message',
        '#message' => $message,
        '#commerce_product_variation' => $variation,
        '#commerce_product_variation_url' => $variation->toUrl(),
        '#user' => $user,
        '#mail' => $data->getEmail(),
      ];
      $body = (string) DeprecationHelper::backwardsCompatibleCall(
        \Drupal::VERSION, '10.3.0',
        fn () => $this->renderer->renderInIsolation($build),
        fn () => $this->renderer->renderPlain($build),
      );
      $params = [
        'mail_title' => $subject,
        'message' => $body,
      ];

      // Use default site language for anonymous users.
      if (empty($user) || $user->isAnonymous()) {
        $language = \Drupal::languageManager()->getDefaultLanguage()->getId();
      } else {
        $language = $user->getPreferredLangcode();
      }

      // Send a message.
      $this->mailManager->mail('commerce_stock_notifications', 'commerce_stock_notifications.notify', $data->getEmail(), $language, $params);
      $time = new DrupalDateTime();

      $data->set('sent_time', $time->getTimestamp())->save();
    }
  }
}
