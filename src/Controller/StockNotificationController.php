<?php

namespace Drupal\commerce_stock_notifications\Controller;

use Drupal\commerce_stock_notifications\Entity\StockNotificationInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Class of StockNotificationController.
 *
 * @package Drupal\commerce_stock_notifications\Controller
 */
class StockNotificationController extends ControllerBase {

  /**
   * Unsubscribe the user from the notification.
   *
   * @param string $user
   *   User ID.
   * @param string $notification_id
   *   Notification ID.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Redirect.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   Thrown when the underlying entity cannot be deleted.
   */
  public function unsubscribe(string $user, string $notification_id): RedirectResponse {
    /** @var \Drupal\commerce_stock_notifications\Entity\StockNotification $notification */
    $notification = $this->entityTypeManager()->getStorage('commerce_stock_notification')->load($notification_id);

    if (!$notification instanceof StockNotificationInterface) {
      return new RedirectResponse('/system/404');
    }
    // Check if the current user can delete the notification, and if so, delete
    // the notification below.
    if (!$notification->access('delete')) {
      return new RedirectResponse('/system/403');
    }

    $notification->delete();

    /** @var \Drupal\user\UserInterface $user */
    $user = $this->entityTypeManager()->getStorage('user')->load($user);
    $this->messenger()->addStatus($this->t('Successfully unsubscribed.'), $user);

    $path = Url::fromRoute('view.list_of_subscribed_products.page_1', [
      'user' => $user->id(),
    ])->toString();
    return new RedirectResponse($path);
  }

}
