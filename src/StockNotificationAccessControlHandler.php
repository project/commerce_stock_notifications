<?php

namespace Drupal\commerce_stock_notifications;

use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;

/**
 * Access controller for the Stock notification entity.
 *
 * @see \Drupal\commerce_stock_notifications\Entity\StockNotification.
 */
class StockNotificationAccessControlHandler extends EntityAccessControlHandler {

  /**
   * Permission for administering stock notifications.
   */
  const ADMIN_PERMISSION = 'administer commerce stock notifications';

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account) {
    switch ($operation) {
      case 'view':
        return AccessResult::allowedIfHasPermission($account, self::ADMIN_PERMISSION);

      case 'delete':
        if ($account->hasPermission(self::ADMIN_PERMISSION)) {
          return AccessResult::allowed();
        }
        return AccessResult::allowedIf(!$account->isAnonymous() && $account->id() === $entity->getOwnerId());
    }
    return AccessResult::allowed();
  }

}
