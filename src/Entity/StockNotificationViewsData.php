<?php

namespace Drupal\commerce_stock_notifications\Entity;

use Drupal\views\EntityViewsData;

/**
 * Provides Views data for Stock notification entities.
 */
class StockNotificationViewsData extends EntityViewsData {

  /**
   * {@inheritdoc}
   */
  public function getViewsData() {
    $views_data = parent::getViewsData();
    $views_data['commerce_stock_notification']['sent_time']['field']['id'] = 'date';
    return $views_data;
  }

}
