<?php

namespace Drupal\commerce_stock_notifications\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface for defining Stock notification entities.
 *
 * @ingroup commerce_stock_notifications
 */
interface StockNotificationInterface extends ContentEntityInterface, EntityOwnerInterface {

  /**
   * Gets the email address that the email will be sent to.
   *
   * @return string
   *   The email address.
   */
  public function getEmail(): string;

  /**
   * Set an email address that the email will be sent to.
   *
   * @param string $email
   *   Email address.
   *
   * @return StockNotificationInterface
   *   Return an instance of the interface.
   */
  public function setEmail(string $email): StockNotificationInterface;

}
